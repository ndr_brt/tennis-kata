# Tennis Kata
The following is a TDD Kata, an exercise in coding, refactoring and test-first, that you should apply daily for at least 15-30 minutes.

## Before you start
* This Kata is about implementing a simple tennis game.
* Try not to read ahead.
* Do one task at a time. The trick is to learn to work incrementally.

## Scoring System in Tennis
1. Each player can have either of these points in one game 0 15 30 40
2. If you have 40 and you win the ball you win the game, however there are special rules.
3. If both have 40 the players are deuce:
    * If the game is in deuce, the winner of a ball will have advantage and game ball. 
    * If the player with advantage wins the ball he wins the game.
    * If the player without advantage wins they are back at deuce.

For more details click [here](https://en.wikipedia.org/wiki/Tennis#Scoring)

## The kata

### Step 1
A game is won by the first player to have won at least four points in total and at least two points more than the opponent.

### Step 2
The running score of each game is described in a manner peculiar to tennis: scores from zero to three points are described as “love”, “fifteen”, “thirty”, and “forty” respectively.

### Step 3
If at least three points have been scored by each player, and the scores are equal, the score is “deuce”.

### Step 4
If at least three points have been scored by each side and a player has one more point than his opponent, the score of the game is “advantage” for the player in the lead.

## General requirements
- Use whatever language and frameworks you want. Use something that you know well.
- Provide a README with instructions on how to compile and run the application.

**IMPORTANT:**  Implement the requirements focusing on **writing the best code** you can produce.

**CODE SUBMISSION:** Add the code to your own account and send us the link.
